import React from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { List } from './components/list'
import { Details } from './components/details';


function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<List />} />
          <Route path='/details/:name' element={<Details />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
