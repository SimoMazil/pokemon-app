import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Item } from './item';
import { Search } from './search';
import styles from './list.module.sass';

type pokemon = {
  name: string
}

export const List = () => {
  const [pokemons, setPokemons] = useState<pokemon[]>([]);
  const [search, setSearch] = useState<pokemon[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [offset, setOffset] = useState<number>(0);
  const observer = useRef<IntersectionObserver>();
  
  const ReferenceLastItem = useCallback((node: HTMLDivElement) => {
    if(loading) return;
    if(observer.current) observer.current.disconnect();
    observer.current = new IntersectionObserver(entries => {
      if(entries[0].isIntersecting) {
        setOffset(prev => prev + 20);        
      }
    });
    if(node) observer.current.observe(node);
  }, []);

  useEffect(() => {
    setLoading(true);
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=20&offset=${offset}`)
    .then(response => {
      if(!response.ok) throw new Error('Error!', { cause: { response } });
      return response.json();
    })
    .then(data => {
      setTimeout(() => {
        setPokemons(prev => [...prev, ...data.results]);
        setLoading(false);
      }, 1000)
    })
    .catch(e => console.log(e.cause))
  }, [offset]);

  const handleSearchPokemon = (searchResult: pokemon[]) => {
    setSearch(searchResult);
  }

  const renderPokemons = () => {
    const result = !search.length ? pokemons : search;
    const items = result.map((pokemon, index) => {
      if(pokemons.length === index + 1) {
        return <Item reference={ReferenceLastItem} key={index} name={pokemon.name} />
      }
      return <Item reference={() => {}} key={index} name={pokemon.name} />
    })
    return items;
  }

  return (
    <div className={styles.container} data-testid='list'>
      <Search handleSearchPokemon={handleSearchPokemon}/>
      { renderPokemons() }
      { loading && <p className={styles.loading}>loading...</p> }
    </div>
  );
}