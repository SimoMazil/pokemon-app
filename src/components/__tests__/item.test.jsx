import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import { Item } from '../item';

test('test item content', () => {
  render(<Item name='Pikachu'/>, { wrapper: BrowserRouter })
  const itemElement = screen.getByTestId('item');
  expect(itemElement).toBeDefined();
  expect(itemElement.textContent).toContain('Pikachu');
  expect(itemElement.textContent).toContain('Details');
});

test('match item component snapshot', () => {
  const tree = renderer.create(<BrowserRouter><Item /></BrowserRouter>).toJSON();
  expect(tree).toMatchSnapshot();
});