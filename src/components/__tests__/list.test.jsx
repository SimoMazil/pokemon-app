import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import { List } from '../list';

test('test list content', () => {
  render(<List />, { wrapper: BrowserRouter })
  const listElement = screen.getByTestId('list');
  expect(listElement).toBeDefined();
  expect(listElement.textContent).toContain('loading...');
});

test('match list component snapshot', () => {
  const tree = renderer.create(<BrowserRouter><List /></BrowserRouter>).toJSON();
  expect(tree).toMatchSnapshot();
});