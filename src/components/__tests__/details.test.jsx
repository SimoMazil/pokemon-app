import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import { Details } from '../details';

test('test details content', () => {
  render(<Details />, { wrapper: BrowserRouter })
  const detailsElement = screen.getByTestId('details');
  expect(detailsElement).toBeDefined();
  expect(detailsElement.textContent).toContain('loading...');
});

test('match details component snapshot', () => {
  const tree = renderer.create(<BrowserRouter><Details /></BrowserRouter>).toJSON();
  expect(tree).toMatchSnapshot();
});