import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer';
import { Search } from '../search';

test('test search content', () => {
  render(<Search name='Pikachu'/>, { wrapper: BrowserRouter })
  const searchElement = screen.getByTestId('search');
  expect(searchElement).toBeDefined();
});

test('match search component snapshot', () => {
  const tree = renderer.create(<BrowserRouter><Search /></BrowserRouter>).toJSON();
  expect(tree).toMatchSnapshot();
});