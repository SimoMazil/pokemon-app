import styles from './item.module.sass';

interface Props {
    name: string,
    reference: (node: HTMLDivElement) => void
}

export const Item = ({ name, reference }: Props) => {
  return (
  <div ref={reference} className={styles.card} data-testid='item'>
    <h1 className={styles.pokemonName}>{name}</h1>
    <a href={`/details/${name}`} className={styles.pokemonDetails}>Details</a>
  </div>
  )
}