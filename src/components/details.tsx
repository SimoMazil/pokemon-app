import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import styles from './details.module.sass';

type pokemonDetails = {
  name: string,
  height: number,
  base_experience: number,
  weight: number,
  sprites: any
}

export const Details = () => {
  const [details, setDetails] = useState<pokemonDetails>();
  const [loading, setLoading] = useState(false);
  const params = useParams();

  useEffect(() => {
    setLoading(true);
    fetch(`https://pokeapi.co/api/v2/pokemon/${params.name}`)
    .then(response => {
      if(!response.ok) throw new Error('Error!', { cause: { response } });
      return response.json();
    })
    .then(data => {
      setTimeout(() => {
        setDetails(data);
        setLoading(false);
      }, 1000)
    })
    .catch(e => console.log(e.cause));
  }, []);

  return (
    <div className={styles.details} data-testid='details'>
      {
        details && 
        <div>
          <img src={details.sprites?.other['official-artwork']?.front_default} />
          <h1 className={styles.title}>{details.name}</h1>
          <p className={styles.parag}>Height: {details.height}</p>
          <p className={styles.parag}>Weight: {details.weight}</p>
          <p className={styles.parag}>Base Experience: {details.base_experience}</p>
        </div>
      }
      { loading && <p className={styles.loading}>loading...</p> }
    </div>
  )
}