import React, { useCallback, useEffect, useState } from 'react';
import styles from './search.module.sass';

type pokemon = {
  name: string
}

interface Props {
  handleSearchPokemon: (e: pokemon[]) => void
}

export const Search = ({ handleSearchPokemon }: Props) => {
  const [searchValue, setSearchValue] = useState('');
  const [error, setError] = useState('');

  useEffect(() => {
    setSearchValue('')
  }, [])

  const handleSearchQuery = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
    setError('');
    fetch(`https://pokeapi.co/api/v2/pokemon/${e.target.value}`)
    .then(response => {
      if(!response.ok) throw new Error('Error!', { cause: { response } });
      return response.json();
    })
    .then(data => {
      const results = data.results ? [] : [data];
      handleSearchPokemon(results);
      setError('');
    })
    .catch(e => {
      switch (e.cause.response?.status) {
        case 404:
          setError('Pokemon Not Found!')
          break;
      }
    });
  }, [searchValue]);

  return (
    <div data-testid='search'>
      <input
        value={searchValue}
        type='text' 
        className={styles.search}
        placeholder='Search Pokemon by Name' 
        onChange={handleSearchQuery}
      />
      { error && <p>{ error }</p> }
    </div>
  )
}