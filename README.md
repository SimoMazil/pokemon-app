# pokemon-app

A simple pokemon list.

## The front-end

The frontend of the this app is built with :

* **Vite**
* **React JS**
* **TypeScript**

## Usage

To start th project run the following :

```
npm install

npm run dev
```


